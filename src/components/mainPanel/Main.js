import React, { Component } from 'react';
import './main.css';
import '../../lib/bootstrap-3.3.7-dist/css/bootstrap.min.css';
import * as contentMapTmp from '../../source/contentMap.json';
import * as presTmp from '../../source/pres.json'
import BrowseContentPanel from "../browseContentPanel/containers/BrowseContentPanel";
import './main.css'
import Presentation from "../common/presentation/containers/Presentation";

import {updateContentMap,updatePresentation} from '../../actions';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import globalReducer from '../../reducers';
import EditSlidPanel from "../editSlidPanel/containers/EditSlidPanel";

const store = createStore(globalReducer);


export default class Main extends Component{
    constructor(props) {
        super(props);
        this.state = {
            currentPres:presTmp,
            onlyContent:false
        };

        store.dispatch(updateContentMap(contentMapTmp.default));
        console.log(contentMapTmp.default)
        store.dispatch(updatePresentation(presTmp.default));
    }

    render() {
        return (
            <Provider store={store} >
                <div className='container-fluid height-100'>
                    <div className="row height-100">
                        <div className='col-md-3 col-lg-3 height-100'>
                            <Presentation/>
                        </div>
                        <div className='col-md-6 col-lg-6 height-100'>
                            <EditSlidPanel/>
                        </div>
                        <div className='col-md-3 col-lg-3 height-100'>
                            <BrowseContentPanel/>
                        </div>
                    </div>
                </div>
            </Provider>
        );
    }
}