import React, { Component } from 'react';
import OnlyVisual from "../../content/components/OnlyVisual";
import './slid.css'
import EditMetaSlid from "../components/EditMetaSlid";
import { connect } from 'react-redux';
import {setSelectedSlid, updateSlid} from '../../../../actions'
class Slid extends Component {
    constructor(props) {
        super(props);
        this.updateSelectedSlid=this.updateSelectedSlid.bind(this);
        this.updateSlid=this.updateSlid.bind(this);
        this.handleChangeTxt=this.handleChangeTxt.bind(this);
        this.handleChangeTitle=this.handleChangeTitle.bind(this);
        this.drop=this.drop.bind(this);
        this.allowDrop=this.allowDrop.bind(this);
    }

    updateSelectedSlid(){
        const tmpSlid={
            id:this.props.id,
            title:this.props.title,
            txt:this.props.txt,
            content_id:this.props.content_id
        };
        this.props.dispatch(setSelectedSlid(tmpSlid));
    }

    handleChangeTxt(e) {
        const tmpSlid = {
            id: this.props.id,
            title: this.props.title,
            txt: e.target.value,
            content_id: this.props.content_id
        }
        this.updateSlid(tmpSlid)
    }

    handleChangeTitle(e) {
        const tmpSlid = {
            id: this.props.id,
            title: e.target.value,
            txt: this.props.txt,
            content_id: this.props.content_id

        }
        this.updateSlid(tmpSlid)
    }

    updateSlid(tmpSlid) {
        this.props.dispatch(updateSlid(tmpSlid))
    }

    drop(e) {
        e.preventDefault();
        console.log(this.props)
        const tmpSlid = {
            id: this.props.id,
            title: this.props.title,
            txt: this.props.txt,
            content_id: this.props.dragged
        };
        this.updateSlid(tmpSlid)
    }

    allowDrop(e) {
        e.preventDefault();
    }

    processDisplay() {
        switch(this.props.displayMode) {
            case 'SHORT':
                return (
                    <div onClick={()=>this.updateSelectedSlid()}>
                        <h2>{this.props.title}</h2>
                        <p>{this.props.txt}</p>

                        <OnlyVisual
                            src={this.props.content_map[this.props.content_id].src}
                            type={this.props.content_map[this.props.content_id].type}
                        />
                    </div>
                );
                break;
            case 'FULL_MNG':
                console.log(this.props)

                return (
                    <div onDrop={this.drop} onDragOver={this.allowDrop}>
                        <EditMetaSlid
                            title={this.props.title}
                            txt={this.props.txt}
                            handleChangeTxt={this.handleChangeTxt}
                            handleChangeTitle={this.handleChangeTitle}
                        />
                        <OnlyVisual
                            src={this.props.content_map[this.props.content_id].src}
                            type={this.props.content_map[this.props.content_id].type}
                        />
                    </div>
                )
                break;
        }
    }

    render() {
        let display_result=this.processDisplay();

        return (<div>{display_result}</div>);
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        content_map: state.updateModelReducer.content_map,
        presentation: state.updateModelReducer.presentation,
        dragged: state.selectedReducer.dragged
    }
};

export default connect(mapStateToProps)(Slid);