import React, { Component } from 'react';
import OnlyVisual from "../components/OnlyVisual";
import FullVisual from "../components/FullVisual";
import {updateDraggedElt } from '../../../../actions'
import {connect} from "react-redux";
class Content extends Component {
    constructor(props) {
        super(props);
        this.drag=this.drag.bind(this);
    }

    drag() {
        this.props.dispatch(updateDraggedElt(this.props.id))
    }

    render() {
            if (this.props.onlyContent) {
                return (
                    <div>
                        <OnlyVisual
                            src={this.props.src}
                            type={this.props.type}
                        />
                    </div>
                );
            }
            else {
                return (
                    <div draggable="true"
                         onDragStart={this.drag}>
                        <FullVisual
                            src={this.props.src}
                            type={this.props.type}
                            id={this.props.id}
                            title={this.props.title}
                        />
                    </div>
                );
            }
    }
}

export default connect()(Content);