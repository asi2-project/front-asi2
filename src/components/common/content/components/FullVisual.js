import React, { Component } from 'react';
import OnlyVisual from "./OnlyVisual";
export default class FullVisual extends Component {
    constructor(props) {
        super(props);
    }

    render() {

        return (
            <div>
                <OnlyVisual
                    src={this.props.src}
                    type={this.props.type}
                />
                <p>ID: {this.props.id} - Title: {this.props.title}</p>
            </div>
        );
    }
}