import React, { Component } from 'react';
export default class OnlyVisual extends Component {
    constructor(props) {
        super(props);
    }

    processDisplay() {
        switch(this.props.type) {
            case 'img_url':
                return (
                    <img className="img-thumbnail" src={this.props.src}/>
                );
                break;
            case 'video':
                return (
                    <object  width="100%" height="100%"
                             data={this.props.src}>
                    </object>
                );
                break;
            case 'web':
                return (
                    <iframe src={this.props.src}></iframe>
                );
                break;
        }
    }

    render() {
        let display_result=this.processDisplay();

        return (<div>{display_result}</div>);
    }
}