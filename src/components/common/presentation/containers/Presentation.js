import React from 'react';
import EditMetaPres from "../components/EditMetaPres";
import SlidList from "../components/SlidList";
import {connect} from "react-redux";
class Presentation extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
            <EditMetaPres
                id={this.props.presentation.id}
                title={this.props.presentation.title}
                description={this.props.presentation.description}
            />

            <SlidList
                slidArray={this.props.presentation.slidArray}
            />

            </div>
        );
    }

}

const mapStateToProps = (state, ownProps) => {
    return {
        presentation:state.updateModelReducer.presentation
    }
};

export default connect(mapStateToProps)(Presentation);