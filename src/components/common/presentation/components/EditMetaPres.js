import React from 'react';
export default class EditMetaPres extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="form-group">
                <label htmlFor="presTitle">Title </label>
                <input
                    type="text"
                    className="form-control"
                    id="presTitle"
                    onChange={this.props.handleChangePresTitle}
                    value={this.props.title}
                />
                <label htmlFor="presDescribe">Description</label>
                <textarea
                    rows="5"
                    type="text"
                    className="form-control"
                    id="presDescribe"
                    onChange={this.props.handleChangeDescription}
                    value={this.props.description}>
                    </textarea>
            </div>
        );
    }
}