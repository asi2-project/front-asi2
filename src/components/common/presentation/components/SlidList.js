import React from 'react';
import Slid from "../../slid/containers/Slid";
import {connect} from "react-redux";
class SlidList extends React.Component {
    constructor(props) {
        super(props);

    }

    processDisplay() {
        let array_render=[];
        let slidArray=this.props.slidArray;
        for (let i=0; i < slidArray.length; i++) {
            array_render.push(
                <Slid
                    id={slidArray[i].id}
                    title={slidArray[i].title}
                    txt={slidArray[i].txt}
                    content_id={slidArray[i].content_id}
                    displayMode="SHORT"
                />
            );
        }
        return array_render;
    }

    render() {
        const display_list= this.processDisplay();

        return (
            <div className="vertical-scroll height-70vh">
                {display_list}
            </div>
        );
    }
}

export default connect()(SlidList);