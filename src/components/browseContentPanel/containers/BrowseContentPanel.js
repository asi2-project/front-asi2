import React, { Component } from 'react';
import Content from "../../common/content/containers/Content";
import './browseContentPanel.css'
import {connect} from "react-redux";
import Button from '@material-ui/core/Button';
import Icon from '@material-ui/core/Icon';
import AddContentPanel from "../components/AddContentPanel";
class BrowseContentPanel extends Component {
    constructor(props) {
        super(props);
    }
    state = {
        open: false,
    };

    toggleModal = () => {
        this.setState({
            open: !this.state.open
        });
    }


    render() {
        let contentMap = this.props.content_map;
        return (
                <div>
                    <Button onClick={this.toggleModal} color="primary">
                        <Icon>add</Icon>
                    </Button>
                        <AddContentPanel
                            open={this.state.open}
                            onClose={this.toggleModal}
                        />
                    {Object.keys(contentMap).map((index) => {
                        let content = contentMap[index];
                        return (
                            <Content
                                key={content.id}
                                id={content.id}
                                type={content.type}
                                title={content.title}
                                src={content.src}
                                onlyContent={false}
                            />
                        );
                    })}
                </div>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        content_map: state.updateModelReducer.content_map
    }
};

export default connect(mapStateToProps)(BrowseContentPanel);