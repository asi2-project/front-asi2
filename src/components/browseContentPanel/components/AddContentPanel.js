import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import InputLabel from '@material-ui/core/InputLabel';
import Input from '@material-ui/core/Input';
import DialogTitle from '@material-ui/core/DialogTitle';
import Select from '@material-ui/core/Select';
import {addContent} from "../../../actions";
import {connect} from "react-redux";

class AddContentPanel extends React.Component {
    constructor(props) {
        super(props);
        this.addContent=this.addContent.bind(this);
    }
    state = {
        type: '',
        title:'',
        src:'',
    };


    handleChange = name => event => {
        this.setState({ [name]: event.target.value });
    };

    addContent() {
        const content = {
            title: this.state.title,
            type:this.state.type,
            src: this.state.src
        }
        this.props.dispatch(addContent(content));
        this.props.onClose();
        console.log(this.props)
    }

    render() {
        return (
            <div>
                <Dialog
                    open={this.props.open}
                    aria-labelledby="form-dialog-title"
                >
                    <DialogTitle id="form-dialog-title">Add a new content</DialogTitle>
                    <DialogContent>
                        <TextField
                            autoFocus
                            margin="dense"
                            id="title"
                            label="Title"
                            type="text"
                            value={this.state.title}
                            onChange={this.handleChange('title')}
                            fullWidth

                        />
                        <InputLabel htmlFor="age-native-simple">Content Type</InputLabel>
                        <Select
                            native
                            value={this.state.type}
                            onChange={this.handleChange('type')}
                            input={<Input id="type-native-simple" />}
                        >
                            <option value="" />
                            <option value="img_url">Image URL</option>
                            <option value="video">Video</option>
                            <option value="web">Web</option>
                        </Select>
                        <TextField
                            autoFocus
                            margin="dense"
                            id="src"
                            label="URL"
                            type="text"
                            value={this.state.src}
                            onChange={this.handleChange('src')}
                            fullWidth
                        />
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.props.onClose} color="primary">
                            Cancel
                        </Button>
                        <Button onClick={this.addContent} color="primary">
                            Add
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        );
    }
}

export default connect()(AddContentPanel);