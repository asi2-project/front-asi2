const selectedReducer= (state={slid:{}, dragged:{}},action) => {
    //console.log(action);
    switch (action.type) {
        case 'UPDATE_SELECTED_SLID':
            return Object.assign({}, state, {slid:action.obj});
        case 'UPDATE_DRAGGED':
            return Object.assign({}, state, {dragged:action.obj});
        default:
            return state;
    }
}
export default selectedReducer;